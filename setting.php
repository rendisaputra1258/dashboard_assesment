<?php

include 'connection.php';
// include 'cari.php';


foreach ($data_buku as $key) {
    
}
if(isset($_POST['search']))
{
    
    $name=$_POST['search'];
    
    $search=$db->prepare("select * from buku where judul=? or penulis=? or genre=? or sekolah=?");
    
    $search->bindValue(1, $name,PDO::PARAM_STR);
    $search->bindValue(2, $name,pdo::PARAM_STR);
    $search->bindValue(3, $name,PDO::PARAM_STR);
    $search->bindValue(4, $name,pdo::PARAM_STR);
    
    $search->execute();
    
    $tampil_data=$search->fetchAll();
    
    $row=$search->rowCount();
}

// var_dump($data_buku);


$fiks_data=[];
if(isset($_POST['filter']))
{
    $filter=$_POST['filter'];
    if($filter == "")
    {
        $fiks_data=$data_buku;
    }else{
        foreach($data_buku as $key)
        {
            if($key[0] == $filter){
                $fiks_data[]=[$key[0],$key[1],$key[2]];
            }
        }
    }
}else{
    $fiks_data=$data_buku;
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
      <div class="bdy">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="fontawesome-free/css/all.min.css">
        <title>DAFTAR PESERTA</title>
      </head>
      <style>
        .bdy{
          background-color: rgb(5, 6, 71);
        }
      </style>
    <body class=" mt-5">
    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary fixed-top">
  <a class="navbar-brand" href="#">LOMBA KARYA TULIS| <b>UNIVERSITAS INDONESIA</b></a>
</nav>
<div class="row no-guters mt-5 my-4 ">
    <div class="col-md-2 bg-dark text-light mt-2 pr-3 pt-3 my-2"> 
    <ul class="nav flex-column ml-3 mb-5">
    <li class="nav-item">
          <a class="nav-link text-light" href="index.php"><i class="fas fa-sign-in-alt mr-2 text-light"></i>Home</a><hr class="bg-light">
      </li>
      <li class="nav-item">
          <a class="nav-link text-light" href="dashboard.php"><i class="fas fa-sign-in-alt mr-2 text-light"></i>Dashboard</a><hr class="bg-light">
      </li>
    <li class="nav-item">
        <a class="nav-link active text-light" href="peserta.php"><i class="fas fa-hotel mr-2 text-light"></i>informasi</a><hr class="bg-light">
    </li>
    <li class="nav-item">
        <a class="nav-link text-light" href="daftar.php"><i class="fas fa-sign-in-alt mr-2 text-light"></i>pendaftaran</a><hr class="bg-light">
    </li>
    <li class="nav-item">
        <a class="nav-link text-light" href="setting.php"><i class="fas fa-sign-in-alt mr-2 text-light"></i>setting</a><hr class="bg-light">
    </li>
    </ul>
    </div>
    <div class="col-md-10 col-sm-4 p-4 text-center ">           
       <div class="container">  
       <div class="text-center">
         <div class="bg-secondary">
            <h1 class="text-light" style="font-family: calibry;">Data peserta</h1>                
                <table class="table table-striped btn-light">
                   <thead class="bg bg-light">
                <tr >
             <th scope="col">ID BUKU</th>
            <th scope="col">JUDUL</th>
            <th scope="col">PENULIS</th>
            <th scope="col">GENRE</th>
            <th scope="col">SEKOLAH</th>
            <th scope="col">AKSI</th>
         </tr>
       </thead>
     </div>
    </div>
  </div>
    <tbody>
        <?php foreach ($data_buku as $key):?>
            <tr>
                <td><?php echo $key['id_buku'];?></td>
                <td><?php echo $key['judul'];?></td>
                <td><?php echo $key['penulis'];?></td>
                <td><?php echo $key['genre'];?></td>
                <td><?php echo $key['sekolah'];?></td>   
             <td><a onclick="return confirm ('apakah anda ingin mengapus data ini?')" href="delete.php?id_buku=<?php echo $key['id_buku'];  ?>"  class="btn btn-danger">Hapus</a> 
                 <a  href="edit.php?id_buku=<?php echo $key['id_buku']; ?>"  class="btn btn-primary">Edit</a></td>
            </tr>   
            <?php endforeach; ?>
        </tbody> 
    </table>
    
    <footer>
       <div class="text-center text-light">
          @ 2020 Aplikasasi crud: rendi saputra
               </div>
                <div class="clearfix">
                  </div>
                </footer>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
</body>
</html>