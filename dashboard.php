<?php


include 'connection.php';

foreach ($data_buku as $key) {
    
}

if(isset($_POST['search']))
{ 
    $name=$_POST['search'];
    
    $search=$db->prepare("select * from buku where judul=? or penulis=?");
    
    $search->bindValue(1, $name,PDO::PARAM_STR);

    $search->bindValue(2, $name,pdo::PARAM_STR);
    
    $search->execute();
    
    $tampil_data=$search->fetchAll();
    
    $row=$search->rowCount();
}
// var_dump($data_buku);
$fiks_data=[];
if(isset($_POST['filter']))
{
    $filter=$_POST['filter'];
    if($filter == "")
    {
        $fiks_data=$data_buku;
    }else{
        foreach($data_buku as $key)
        {
            if($key[0] == $filter){
                $fiks_data[]=[$key[0],$key[1],$key[2]];
            }
        }
    }
}else{
    $fiks_data=$data_buku;
}
$kuota = 50 ;
$temp_data = count($data_buku);
$temp_kosong = $kuota - $temp_data;

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://kit.fontawesome.com/a540d7261a.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
      .bdy{
         background-color: rgb(9, 9, 65);      }
    </style>

    <title>Dashboard</title>
  </head>
  <body>
    <div class="bdy">
  <nav class="navbar navbar-expand-lg navbar-dark bg-secondary fixed-top">
  <a class="navbar-brand text-light">LOMBA KARYA TULIS| <b>UNIVERSITAS INDONESIA</b></a>
</nav>
<div class="row no-guters mt-5 my-4">
    <div class="col-md-2 bg-dark text-light mt-2 pr-3 pt-3 my-2"> 
      <div class="d-flex justify-content-center">
      <img src="image/Makara_UI.png" alt="" height="75px" width="75px">
      </div>
    <ul class="nav flex-column ml-3 mb-5">
      <li class="nav-item">
          <a class="nav-link text-light" href="index.php"><i class="fab fa-accusoft mr-2 text-light"></i>Home</a><hr class="bg-light">
      </li>
      <li class="nav-item">
          <a class="nav-link text-light" href="dashboard.php"><i class="fas fa-desktop mr-2 text-light"></i>Dashboard</a><hr class="bg-light">
      </li>
      <li class="nav-item">
        <a class="nav-link active text-light" href="peserta.php"><i class="fas fa-hotel mr-2 text-light"></i>informasi</a><hr class="bg-light">
    </li>
    <li class="nav-item">
        <a class="nav-link text-light" href="daftar.php"><i class=" fas fa-sign-in-alt mr-2 text-light"></i>pendaftaran</a><hr class="bg-light">
    </li>
    <li class="nav-item">
        <a class="nav-link text-light" href="setting.php"><i class="fas fa-users-cog mr-2 text-light"></i>setting</a><hr class="bg-light">
    </li>
    </ul>
    <footer>
       <div class="text-center text-light">
          @ 2020rendi saputra
               </div>
                <div class="clearfix">
               </div>
        </footer>
    </div>
    <div class="col-md-10 p-4 text-light">
       <h3><i class="fas fa-desktop mr-2 text-light"></i>Dashboard</a><hr class="bg-dark"></h3>    
         <?php if(isset($row)):?>
              <div class="alert alert-primary alert-dismissible fade show mt-3" role="alert">
                <p class="lead"><?php echo $row; ?>Data Ditemukan</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <?php endif;?>
              <div class="row">
                <div class="col-sm-4 my-1">
              <div class="card text-center" style="width: 18rem;">
                <div class="card-body  bg-secondary text-light">
                  <h5 class="card-title">kuota pendaftaran</h5>
                  <h6 class="card-subtitle mb-2 text-light"><i class="fas fa-users mr-2"></i>batas kuota</h6>
                  <p class="card-text"><?php echo $kuota;?></p>
                </div>
                </div>
                </div>
                  <div class="col-sm-4 my-1">
                <div class="card text-center" style="width: 18rem;">
                  <div class="card-body  bg-secondary text-light">
                    <h5 class="card-title">peserta</h5>
                    <h6 class="card-subtitle mb-2 text-light"><i class="fas fa-user-check mr-2"></i>terdaftar</h6>
                    <p class="card-text"><?php echo $temp_data;?></p>
                  </div>
                 </div>
                </div>
                  <div class="col-sm-4 my-1">
                <div class="card text-center" style="width: 18rem;">
                  <div class="card-body  bg-secondary text-light">
                    <h5 class="card-title">sisa kuota pendaftaran</h5>
                    <h6 class="card-subtitle mb-2 text-light"><i class="fas fa-user-check mr-2"></i>belum terisi</h6>
                    <p class="card-text"><?php echo $temp_kosong;?></p>
                  </div>
                </div>
                </div>
                </div>
                <h4>Data peserta</h4>
                 <div class="text-left mt-3"  style="overflow-y: scroll;height:300px;">
                      <table class="table table-striped btn-light border border-rounded">
                        <thead class="bg-secondary">
                      <tr class="text-light" >
                        <th scope="col">ID BUKU</th>
                          <th scope="col">JUDUL</th>
                          <th scope="col">PENULIS</th>
                          <th scope="col">GENRE</th>
                          <th scope="col">SEKOLAH</th>
                      </tr>
                    </thead>
                  </div>
                </div>
                <tbody>
                  <?php $urut=1; foreach ($data_buku as $key):?>
                    <tr>
                      <td><?php echo $urut;?></td>
                      <td><?php echo $key['judul'];?></td>
                      <td><?php echo $key['penulis'];?></td>
                      <td><?php echo $key['genre'];?></td>
                      <td><?php echo $key['sekolah'];?></td>   
                    </tr>   
                    <?php $urut++; endforeach; ?>
                  </tbody> 
                </table>
              </div>
          </div>
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>