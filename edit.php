 <?php
include 'connection.php';

$data=$db->prepare('select * from buku where id_buku=?');

$data->bindValue(1,$_GET['id_buku']);

$data->execute();

$data_buku=$data->fetchAll();         
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Form Edit buku</title>
</head>
<body class="mt-10 bg-info">
        <div class="container mt-5 ">
          <div class="row vh-50 justify-content-center"> 
            <!-- <div class="col-10  "> -->
              <div class="col-8 bg-light p-3 ">
                <h1 class="text-center text-dark " style="font-family: calibry; ">Edit data</h1>
            <form action="update.php" method="POST">
               <div class="form-group"> 
            <form action="update.php" method="POST"> 
                <input type="hidden" required name="id_buku"  value="<?php echo $data_buku[0]['id_buku']; ?>"> 
             <div class="form-group">
                <label for="exampleInputPassword1">Judul buku</label>
                <input type="text" required  name="judul" value="<?php echo $data_buku[0]['judul']; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Penulis</label>
                <input type="text" required  name="penulis" value="<?php echo $data_buku[0]['penulis']; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Genre</label>
                <input type="text" required  name="genre" value="<?php echo $data_buku[0]['genre']; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">sekolah</label>
                <input type="text" required  name="sekolah" value="<?php echo $data_buku[0]['sekolah']; ?>" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>